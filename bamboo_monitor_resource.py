import socket

from bitbucket_client import BitbucketClient
from flask import Flask
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)
api = Api(app)


class PullRequests(Resource):
    def get(self):
        return BitbucketClient.get_pull_requests()


# Adding the pullrequests resource
api.add_resource(PullRequests, '/pullrequests')  # retrieve BitBucket pull requests


# Method to retrieve the server's IP
def get_ip_address():
    ip_address = ''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_address = s.getsockname()[0]
    s.close()
    return ip_address


# Running the app on the specified port. You can use the get_ip_address method to run the app on the server's IP
if __name__ == '__main__':
    app.run(host=get_ip_address(), port='5001')
