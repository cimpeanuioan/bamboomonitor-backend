# Bamboo Monitor - backend

## Service written in Python which exposes an endpoint providing all the information regarding pull requests for a
certain repository (GIT)

## Endpoint:

GET /pullrequests

Sample JSON response:

```json
[
  {
    "author": "cimpeanuioan",
    "avatar": "https://bitbucket.org/account/cimpeanuioan/avatar/32/",
    "comments_count": 1,
    "destination": "master",
    "source": "new_feature",
    "state": "OPEN",
    "title": "Added a new feature"
  }
]
```