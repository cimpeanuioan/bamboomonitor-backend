import requests
import json
from flask import jsonify


# [STASH-BASE-URL]
url = 'https://api.bitbucket.org/2.0/repositories/cimpeanuioan/bamboo-monitor/pullrequests?state="all"'
headers = {'Content-Type': 'application/json'}


class BitbucketClient:
    def __init__(self):
        pass

    @staticmethod
    def custom_key(pr):
        if pr['state'] == 'OPEN':
            return 1
        elif pr['state'] == 'DECLINED':
            return 2
        elif pr['state'] == 'MERGED':
            return 3
        else:
            return 4
        

# Client method to retrieve and parse the pull requests call to Stash
    @staticmethod
    def get_pull_requests():
        r = requests.get(url)
        data = json.loads(r.text)
        pr_received = data['values']
        pull_requests = []
        for pr in pr_received:
            title = pr['title']
            state = pr['state']
            destination = pr['destination']['branch']['name']
            source = pr['source']['branch']['name']
            author = pr['author']['display_name']
            avatar = pr['author']['links']['avatar']['href']
            comments_count = pr['comment_count']
            pull_requests.append({'title': title,
                                  'state': state,
                                  'destination': destination,
                                  'source': source,
                                  'author': author,
                                  'avatar': avatar,
                                  'comments_count': comments_count})

        return jsonify(sorted(pull_requests, key=lambda pull_requests: BitbucketClient.custom_key(pull_requests)))


